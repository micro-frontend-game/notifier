# Notifier

## Problemas Conhecidos
Caso rodar os testes e2e resulte em um erro `The futex facility returned an unexpected error code.`,
pode ser questão das permissões do Xorg local. O comando `xhost local:` libera permissão para conexões
locais usarem o Xorg.
