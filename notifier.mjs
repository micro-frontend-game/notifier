"use strict";

import BaseComponent from '../base-component/BaseComponent.mjs'

export default class Notifier extends BaseComponent {

  __notifications = {}
  __lastId = 0
  __delayedNotificationRenderQueue = []

  style = `
    #notifier-box {
      transition: 0.5s;
      position: fixed;
    }
    .notifier-position-top, .notifier-position-top-left, .notifier-position-top-right {
      top: 5px;
    }
    .notifier-position-bottom, .notifier-position-bottom-left, .notifier-position-bottom-right {
      bottom: 5px;
    }
    .notifier-position-top, .notifier-position-bottom {
      left: 50%;
      transform: translate(-50%);
    }
    .notifier-position-top-right, .notifier-position-bottom-right {
      right: 5px;
    }
    .notifier-position-top-left, .notifier-position-bottom-left {
      left: 5px;
    }
    #notifier-box > ul {
      margin: 0;
      padding: 0;
    }
    li.notification {
      margin: 8px 0;
      padding: 8px;
      list-style: none;
      background: #CCC;
      border-radius: 7px;
      box-shadow: 2px 2px 6px rgba(0,0,0,.5);
      cursor: pointer;
    }
    .notification h3 {
      margin: 0;
    }
    .notification p {
      margin: 0;
    }
  `

  template = `
    div#notifier-box.notifier-position-top
      ul
  `

  constructor() {
    super()
  }

  __afterCreate() {
    this.shared.bus.on('notify', this.createNotification.bind(this))
  }

  afterConnected() {
    this.__notificationList = this.__root.querySelector('#notifier-box ul')
    while (this.__delayedNotificationRenderQueue.length) {
      this.__delayedNotificationRenderQueue.shift().call()
    }
  }

  get notifierBox() {
    return this.__root.getElementById('notifier-box')
  }

  static get observedAttributes() { return ['position']; }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'position') {
      this.notifierBox.classList.remove(`notifier-position-${oldValue||'top'}`)
      this.notifierBox.classList.add(`notifier-position-${newValue||'top'}`)
    }
  }

  __createTitle(title) {
    const element = document.createElement('h3')
    element.append(title)
    return element
  }

  __createBody(body) {
    const element = document.createElement('p')
    element.append(body)
    return element
  }

  __renderNotification(notification) {
    notification.element.append(
      this.__createTitle(notification.message.title),
      this.__createBody(notification.message.body)
    )
    notification.element.classList.add('notification')
    if (this.__notificationList) {
      this.__notificationList.appendChild(notification.element)
    }
    else {
      throw Error('notificationList not ready')
    }
  }

  destroyNotification(id) {
    const notification = this.__notifications[id]

    if (this.__notificationList) {
      this.__notificationList.removeChild(notification.element)
    }

    delete this.__notifications[id]
  }

  __normalizeMessage(message) {
    let normalized = {
      timeout: 60 * 1000, // one minute
      title: '',
      body: ''
    }

    if (typeof message === 'string') {
      normalized.body = message
    }
    else {
      normalized = { ...normalized, ...message }
    }

    return normalized
  }

  createNotification(message) {
    const notification = {
      message: this.__normalizeMessage(message),
      element: document.createElement('li')
    }

    const id = this.__registerNotification(notification)
    notification.element.addEventListener('click', () => {
      clearTimeout(this.__notifications[id].destroyTimeoutId)
      this.destroyNotification(id)
    })

    const task = () => {
      this.__renderNotification(notification)
      this.__addDestroyTimeout(id, notification.message.timeout)
    }
    this.__notificationList ? task() : this.__delayedNotificationRenderQueue.push(task)
  }

  __registerNotification(notification) {
    this.__notifications[++this.__lastId] = notification
    return notification.id = this.__lastId
  }

  __addDestroyTimeout(id, time) {
    this.__notifications[id].destroyTimeoutId = setTimeout(() => this.destroyNotification(id), time)
  }

}
