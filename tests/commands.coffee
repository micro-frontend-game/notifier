Cypress.Commands.add 'findInShadow', prevSubject: 'element', (el, query) =>
  cy.get query, withinSubject: el[0].shadowRoot

Cypress.Commands.add 'bbox', prevSubject: 'element', (el) =>
  # TODO: esse metodo não responde de forma confiável, pois os metodos de mensuração
  # do jQuery desconsideram margem e padding. É preciso reimplementar usando os metodos
  # do vanilla.
  cy.get('body').then (body)=>
    { top, left, bottom, right, width, height } = el[0].getBoundingClientRect()
    bbox = {
      top,
      left,
      bottom: body[0].clientHeight - bottom,
      right: body[0].clientWidth - right,
      width,
      height
    }
    cy.wrap bbox
