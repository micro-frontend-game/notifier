import loadComponent from '../node_modules/loader/loader.mjs'
import buildBus from '../node_modules/bus/bus.mjs'
import NotifierBase from '../notifier.mjs'

NotifierBase.forceOpenShadow = true

getNotifierBox = (extraQuery='')->
  cy.get(@Notifier.Component.tagName).findInShadow '#notifier-box ' + extraQuery

describe 'Notification micro-component', ->

  beforeEach ->
    @getNotifierBox = getNotifierBox
    @bus = buildBus()
    cy.wait(1).then ()=>
      loadComponent(NotifierBase, {bus: @bus}).then (@Notifier)=>
    cy.window().then (@win)=>
      @doc = @win.document
      @doc.body.innerHTML = ''

  it 'builds', ->
    @Notifier.create parent: @doc.body
    do @getNotifierBox

  it 'shows a simple notification with just a string', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', '''I'm a notification!'''
    @getNotifierBox('.notification')
      .contains '''I'm a notification!'''

  it 'destroys the notification after the timeout', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', body: 'Hi you!', timeout: 250
    @getNotifierBox().contains 'Hi you!'
    cy.wait 300
    @getNotifierBox().contains('Hi you!').should 'not.exist'

  it 'Notification with body and title', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', title: 'A Notification', body: 'Hi you!'
    @getNotifierBox('.notification h3').contains 'A Notification'
    @getNotifierBox('.notification p').contains 'Hi you!'

  it 'positions notification on top-center', ->
    @Notifier.create parent: @doc.body, position: 'top'
    @bus.send 'notify', 'Message on top.'
    @getNotifierBox().bbox().its('top').should 'be.equal', 5
    @getNotifierBox().bbox().its('right').then (right)=>
      @getNotifierBox().bbox().its('left').should 'be.equal', right

  it 'positions notification on top-left', ->
    @Notifier.create parent: @doc.body, position: 'top-left'
    @bus.send 'notify', 'Message on top left.'
    @getNotifierBox().bbox().its('top').should 'be.equal', 5
    @getNotifierBox().bbox().its('left').should 'be.equal', 5

  it 'positions notification on top-right', ->
    @Notifier.create parent: @doc.body, position: 'top-right'
    @bus.send 'notify', 'Message on top right.'
    @getNotifierBox().bbox().its('top').should 'be.equal', 5
    @getNotifierBox().bbox().its('right').should 'be.equal', 5

  it 'positions notification on bottom-center', ->
    @Notifier.create parent: @doc.body, position: 'bottom'
    @bus.send 'notify', 'Message on bottom.'
    @getNotifierBox().bbox().its('bottom').should 'be.equal', 5
    @getNotifierBox().bbox().its('right').then (right)=>
      @getNotifierBox().bbox().its('left').should 'be.equal', right

  it 'positions notification on bottom-left', ->
    @Notifier.create parent: @doc.body, position: 'bottom-left'
    @bus.send 'notify', 'Message on bottom-left.'
    @getNotifierBox().bbox().its('bottom').should 'be.equal', 5
    @getNotifierBox().bbox().its('left').should 'be.equal', 5

  it 'positions notification on bottom-right', ->
    @Notifier.create parent: @doc.body, position: 'bottom-right'
    @bus.send 'notify', 'Message on bottom-right.'
    @getNotifierBox().bbox().its('bottom').should 'be.equal', 5
    @getNotifierBox().bbox().its('right').should 'be.equal', 5

  it 'should display multiple notifications', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', 'Message 1.'
    @bus.send 'notify', 'Message 2.'
    @bus.send 'notify', 'Message 3.'
    @getNotifierBox('.notification').contains('Message 1.')
    @getNotifierBox('.notification').contains('Message 2.')
    @getNotifierBox('.notification').contains('Message 3.')

  it 'should destroy notifications in order, from oldest to newest', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', body: 'Message 1.', timeout: 400
    cy.wait(100).then =>
      @bus.send 'notify', body: 'Message 2.', timeout: 400
    cy.wait(100).then =>
      @bus.send 'notify', body: 'Message 3.', timeout: 400

    @getNotifierBox('.notification').contains('Message 1.')
    @getNotifierBox('.notification').contains('Message 2.')
    @getNotifierBox('.notification').contains('Message 3.')
    cy.log('The first dies')
    @getNotifierBox('.notification').contains('Message 1.').should('not.exist')
    @getNotifierBox('.notification').contains('Message 2.')
    @getNotifierBox('.notification').contains('Message 3.')
    cy.log('The second dies')
    @getNotifierBox('.notification').contains('Message 1.').should('not.exist')
    @getNotifierBox('.notification').contains('Message 2.').should('not.exist')
    @getNotifierBox('.notification').contains('Message 3.')
    cy.log('The third dies')
    @getNotifierBox('.notification').should('not.exist')

  it 'style provide a gap between messages', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', body: 'Message 1.'
    @bus.send 'notify', body: 'Message 2.'
    @bus.send 'notify', body: 'Message 3.'
    MSG_HEIGHT = 34
    MSG_MARGIN = 8
    @getNotifierBox('.notification:first-child').invoke('offset').its('top')
      .should 'be.equal', 5 + MSG_MARGIN
    @getNotifierBox('.notification:first-child')
      .then (jq)-> jq[0].getBoundingClientRect().height
      .should 'be.equal', MSG_HEIGHT
    @getNotifierBox('.notification:nth-child(2)').invoke('offset').its('top')
      .should 'be.equal', 5 + MSG_MARGIN + MSG_HEIGHT + MSG_MARGIN

  it 'should erase a notification when it\'s clicked', ->
    @Notifier.create parent: @doc.body
    @bus.send 'notify', body: 'Message 1.'
    @bus.send 'notify', body: 'Message 2.'
    @bus.send 'notify', body: 'Message 3.'
    @getNotifierBox('.notification').its('length').should('be.equal', 3)
    @getNotifierBox('.notification:nth-child(2)').click()
    @getNotifierBox('.notification').contains('Message 1.')
    @getNotifierBox('.notification').contains('Message 2.').should('not.exist')
    @getNotifierBox('.notification').contains('Message 3.')
